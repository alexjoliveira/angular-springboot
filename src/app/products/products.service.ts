import { Product } from './product';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private readonly API = 'http://localhost:3000/products';

  constructor(private http: HttpClient) { }

  findAll() {
    return this.http.get<Product[]>(this.API);
  }
  
  findById(id) {
    return this.http.get(`${this.API}/${id}`);
  }

  insert(product) {
    return this.http.post(this.API, product);
  }
}
